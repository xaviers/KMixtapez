/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

#include <regex>
#include "Backend.h"

void Backend::sendGetRequest(const QString path) {
    request.setUrl(host + path);
    request.setHeader(QNetworkRequest::UserAgentHeader, user_agent);
    request.setRawHeader("cookie", cookie.toUtf8());
    request.setRawHeader("x-mm-verifier", verifier.toUtf8());

    manager->get(request);
}

bool Backend::ready() const {
    return m_ready;
}

bool Backend::hasPlaylist() const {
    return m_has_playlist;
}

QString Backend::playlist() const {
    return m_playlist;
}

bool Backend::shuffle() const {
    return m_shuffle;
}

void Backend::setPlaylist(const QString &playlist) {
    m_playlist = playlist;
    m_has_playlist = true;
    Q_EMIT hasPlaylistChanged();
    Q_EMIT playlistChanged();
}

void Backend::setShuffle(const bool &shuffle) {
    m_shuffle = shuffle;
    Q_EMIT shuffleChanged();
}

std::string Backend::extract_verifier(const std::string &body) {
    std::regex expr("X-MM-VERIFIER&q;,&q;value&q;:&q;([^&]+)&q;");
    std::smatch match;

    if (std::regex_search(body.begin(), body.end(), match, expr)) {
        return match[1].str();
    }

    return "";
}