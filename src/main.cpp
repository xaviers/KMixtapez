/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

#include <KLocalizedContext>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>
#include "Backend.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("Xavier Saliniere");
    QCoreApplication::setOrganizationDomain("xaviers.sh");
    QCoreApplication::setApplicationName("KMixtapez");
    QQmlApplicationEngine engine;

    auto* manager = new QNetworkAccessManager();

    Backend backend(manager);

    qmlRegisterSingletonInstance<Backend>("sh.xaviers.kmixtapez", 1, 0, "Backend", &backend);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return QApplication::exec();
}
