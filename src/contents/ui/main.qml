/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

import QtQuick 2.15
import org.kde.kirigami 2.15 as Kirigami

import sh.xaviers.kmixtapez 1.0

Kirigami.ApplicationWindow {
    id: root

    title: i18n("KMixtapez")

    property int counter: 0

    globalDrawer: GlobalDrawer {}

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: "qrc:/FeaturedPage.qml"

    footer: Player {
        height: Backend.hasPlaylist ? implicitHeight : 0
        visible: Backend.hasPlaylist
    }
}
