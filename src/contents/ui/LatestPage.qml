/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami

import sh.xaviers.kmixtapez 1.0
import "constants.js" as Constants
import "utils.js" as Utils

Kirigami.ScrollablePage {
    id: scrollagePage
    title: i18n("Latest")

    property var loading: true
    property var ready: false
    property var page: 1

    Component.onCompleted: {
        fetchLatestMusic(page);
    }

    Connections {
        target: Backend

        function onReadyChanged() {
            fetchLatestMusic(page);
        }

        function onReplyReceived(url, location, reply) {
            var path = url.toString()
                            .replace(Constants.host, "")
                            .replace(/\?page=[0-9]+/i, "?page=0");

            if (path === Constants.latestMusicPath(0)) {
                JSON.parse(reply)
                    .forEach(e => latestMusicModel.append(Utils.toMusicModel(e)));

                loading = false;
                ready = true;
            }
        }
    }

    ListModel {
        id: latestMusicModel
    }

    AlbumCardsGrid {
        id: cardsGrid
        model: latestMusicModel
        opacity: loading ? 0.3 : 1

        onAtYEndChanged: {
            if (!loading && cardsGrid.atYEnd) {
                page += 1;
                fetchLatestMusic(page);
            }
        }
    }

    Rectangle {
        visible: loading && ready
        width: scrollagePage.width
        height: scrollagePage.height
        color: "black"
        opacity: 0.4
    }
    RowLayout {
        visible: loading
        width: scrollagePage.width
        height: scrollagePage.height

        Controls.BusyIndicator {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: 60
            Layout.preferredHeight: 60
        }
    }

    function fetchLatestMusic(page) {
        if (!Backend.ready) {
            return
        }
        loading = true;

        Backend.sendGetRequest(Constants.latestMusicPath(page));
    }
}
