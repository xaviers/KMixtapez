/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

import QtQuick 2.15
import org.kde.kirigami 2.15 as Kirigami

Kirigami.GlobalDrawer {
    title: i18n("KMixtapez")
    titleIcon: "kirigami-gallery"
    bannerVisible: true
    isMenu: false

    actions: [
        Kirigami.Action {
            text: i18n("Featured Music")
            icon.name: "bookmarks"
            onTriggered: {
                pageStack.clear();
                pageStack.push(Qt.resolvedUrl("FeaturedPage.qml"));
            }
        },
        Kirigami.Action {
            text: i18n("Latest")
            icon.name: "clock"
            onTriggered: {
                pageStack.clear();
                pageStack.push(Qt.resolvedUrl("LatestPage.qml"));
            }
        },
        Kirigami.Action {
            text: "Browse Albums"
            icon.name: "media-album-cover"
            expandible: true

            Kirigami.Action {
                text: i18n("Trending Albums")
                icon.name: "view-statistics"
                onTriggered: {
                    pageStack.clear();
                    pageStack.push(Qt.resolvedUrl("TrendingAlbumsPage.qml"));
                }
            }
            Kirigami.Action {
                text: i18n("Best Albums")
                icon.name: "games-highscores"
                onTriggered: {
                    pageStack.clear();
                    pageStack.push(Qt.resolvedUrl("BestAlbumsPage.qml"));
                }
            }
        },
        Kirigami.Action {
            text: "Browse Singles"
            icon.name: "media-optical-cd-audio-symbolic"
            expandible: true

            Kirigami.Action {
                text: i18n("Trending Singles")
                icon.name: "view-statistics"
                onTriggered: {
                    pageStack.clear();
                    pageStack.push(Qt.resolvedUrl("TrendingSinglesPage.qml"));
                }
            }
            Kirigami.Action {
                text: i18n("Best Singles")
                icon.name: "games-highscores"
                onTriggered: {
                    pageStack.clear();
                    pageStack.push(Qt.resolvedUrl("BestSinglesPage.qml"));
                }
            }
        },
        Kirigami.Action {
            text: i18n("Quit")
            icon.name: "gtk-quit"
            shortcut: StandardKey.Quit
            onTriggered: Qt.quit()
        }
    ]
}
