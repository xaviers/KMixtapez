/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami

import sh.xaviers.kmixtapez 1.0
import "constants.js" as Constants
import "utils.js" as Utils

Kirigami.ScrollablePage {
    id: scrollagePage
    title: i18n("Featured")

    property var loading: true

    Component.onCompleted: {
        fetchFeaturedMusic();
    }

    Connections {
        target: Backend

        function onReadyChanged() {
            fetchFeaturedMusic();
        }

        function onReplyReceived(url, location, reply) {
            var path = url.toString().replace(Constants.host, "");

            if (path === Constants.featuredMusicPath()) {
                JSON.parse(reply)
                    .forEach(e => featuredMusicModel.append(Utils.toMusicModel(e)));

                loading = false;
            }
        }
    }

    ListModel {
        id: featuredMusicModel
    }


    AlbumCardsGrid {
        model: featuredMusicModel
    }

    RowLayout {
        visible: loading
        width: scrollagePage.width
        height: scrollagePage.height

        Controls.BusyIndicator {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: 60
            Layout.preferredHeight: 60
        }
    }

    function fetchFeaturedMusic() {
        if (!Backend.ready) {
            return
        }
        loading = true;

        Backend.sendGetRequest(Constants.featuredMusicPath());
    }
}
