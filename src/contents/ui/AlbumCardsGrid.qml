/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami

Kirigami.CardsGridView {
    minimumColumnWidth: 180
    maximumColumnWidth: 200
    cellHeight: 250

    delegate: Kirigami.Card {
        showClickFeedback: true
        banner {
            source: image
            title: name
            titleLevel: 2
            titleWrapMode: Text.WordWrap
        }
        contentItem: Kirigami.Heading {
            wrapMode: Text.WordWrap
            text: artists
            level: 4
        }
        onClicked: {
            if (isSingle) {
                pageStack.push(Qt.resolvedUrl("SinglePage.qml"), {'singleId': id});
            } else {
                pageStack.push(Qt.resolvedUrl("AlbumPage.qml"), {'albumId': id});
            }
        }
    }
}