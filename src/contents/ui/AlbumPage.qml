/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami

import sh.xaviers.kmixtapez 1.0
import "constants.js" as Constants
import "utils.js" as Utils

Kirigami.ScrollablePage {
    required property var albumId
    property var album
    property var name
    property var loading: true
    title: !!name ? name : ''

    Component.onCompleted: {
        fetchAlbum();
    }

    Connections {
        target: Backend

        function onReadyChanged() {
            fetchAlbum();
        }

        function onReplyReceived(url, location, reply) {
            var path = url.toString().replace(Constants.host, "");

            if (path === Constants.albumPath(albumId)) {
                album = JSON.parse(reply);
                name = album.name;
                album.songs.forEach(song => songModel.append(Utils.toSongModel(song)));

                loading = false;
            }
        }
    }

    ListModel {
        id: songModel
    }

    ListView {
        visible: !loading
        header: Kirigami.ItemViewHeader {
            backgroundImage.source: !!album ? album.images[0].large : ''
            title: !!name ? name : ''
        }
        headerPositioning: ListView.InlineHeader
        focus: false

        model: songModel

        delegate: Kirigami.SwipeListItem {
            Controls.Label {
                text: model.name
            }
            onClicked: {
                var songJson = JSON.stringify([{id: model.id, name: model.name, artists: model.artists}]);
                Backend.playlist = songJson;
            }
        }
    }

    actions {
        main: Kirigami.Action {
            icon.name: "media-playback-start"
            text: i18n("Play")
            enabled: !loading
            onTriggered: {
                var songsJson = JSON.stringify(album.songs.map(song => ({
                    id: song.id,
                    name: song.name,
                    artists: song.artists.main.map(artist => artist.name).join(', ')
                })));
                Backend.shuffle = false;
                Backend.playlist = songsJson;
            }
        }
        right: Kirigami.Action {
            icon.name: "media-playlist-shuffle"
            text: i18n("Shuffle")
            enabled: !loading
            onTriggered: {
                var songsJson = JSON.stringify(album.songs.map(song => ({
                    id: song.id,
                    name: song.name,
                    artists: song.artists.main.map(artist => artist.name).join(', ')
                })));
                Backend.shuffle = true;
                Backend.playlist = songsJson;
            }
        }
    }

    Controls.BusyIndicator {
        visible: loading
    }

    function fetchAlbum() {
        if (!Backend.ready) {
            return
        }
        loading = true;

        Backend.sendGetRequest(Constants.albumPath(albumId));
    }
}
