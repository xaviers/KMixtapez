/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.15 as Kirigami
import QtMultimedia 5.15

import sh.xaviers.kmixtapez 1.0
import "constants.js" as Constants
import "utils.js" as Utils

ColumnLayout {
    property var songs: []
    property var songsLocation: []
    property var loading: false

    Connections {
        target: Backend

        function onPlaylistChanged() {
            loading = true;
            audio.stop();
            playlist.clear();
            songs = JSON.parse(Backend.playlist)
            if (Backend.shuffle) {
                Utils.shuffle(songs);
            }
            songsLocation = songs.map(song => song.id.toString());
            songsLocation.forEach(songId => fetchSongUrl(songId));
        }

        function onReplyReceived(url, location, reply) {
            var path = url.toString().replace(Constants.host, "");

            if (path.indexOf("/stream") !== -1) {
                var songId = path.replace("/api/songs/", "").replace("/stream", "");
                var index = songsLocation.indexOf(songId);
                location = location.replace("/low", "/original");
                songsLocation[index] = location;

                if (isReadyToPlay()) {
                    loading = false;
                    playlist.clear();
                    playlist.addItems(songsLocation);
                    audio.play();
                }
            }
        }
    }

    Audio {
        id: audio
        playlist: Playlist {
            id: playlist
        }
    }

    Kirigami.Separator {
        Layout.fillWidth: true
    }

    RowLayout {
        Layout.preferredWidth: parent.width
        Layout.fillWidth: true
        Layout.bottomMargin: 5

        ColumnLayout {
            Layout.leftMargin: 10
            spacing: -3

            Kirigami.Heading {
                visible: !loading
                text: playlist.currentIndex > -1 ? songs[playlist.currentIndex].name : ""
                level: 3
                type: Kirigami.Heading.Type.Primary
            }
            Kirigami.Heading {
                visible: !loading
                text: playlist.currentIndex > -1 ? songs[playlist.currentIndex].artists : ""
                level: 4
                type: Kirigami.Heading.Type.Secondary
            }

            Controls.BusyIndicator {
                visible: loading
                Layout.preferredHeight: parent.height
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.rightMargin: 10

            Controls.ToolButton {
                Layout.preferredWidth: parent.height
                Layout.fillHeight: true
                display: Controls.AbstractButton.IconOnly
                text: audio.playbackState === Audio.PlayingState ? i18n("Pause") : i18n("Play")
                icon.name: audio.playbackState === Audio.PlayingState ? "media-playback-pause" : "media-playback-start"
                onClicked: audio.playbackState === Audio.PlayingState ? audio.pause() : audio.play()
            }

            Controls.ToolButton {
                Layout.preferredWidth: parent.height
                Layout.fillHeight: true
                display: Controls.AbstractButton.IconOnly
                text: i18n("Next")
                icon.name: "media-skip-forward"
                onClicked: playlist.next()
            }
        }
    }

    function fetchSongUrl(songId) {
        if (!Backend.ready) {
            return
        }
        loading = true;

        Backend.sendGetRequest(Constants.songLocationPath(songId));
    }

    function isReadyToPlay() {
        var ready = true;

        songsLocation.forEach(location => {
            if (location.indexOf("http") === -1) {
                ready = false;
                return;
            }
        });

        if (!loading) {
            return false;
        }

        return ready;
    }
}
