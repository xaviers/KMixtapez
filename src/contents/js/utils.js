function toMusicModel(music) {
    return {
        id: music.id,
        name: music.name,
        image: !music.images ? music.album.images[0].small : music.images[0].small,
        artists: joinArtistsName(music.artists.main),
        isSingle: !!music.album
    }
}

function toSongModel(song) {
    return {
        id: song.id,
        name: song.name,
        artists: joinArtistsName(song.artists.main)
    }
}

function joinArtistsName(artists) {
    return artists.map(artist => artist.name).join(', ');
}

function shuffle(array) {
    var currentIndex = array.length,  randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }

    return array;
}
