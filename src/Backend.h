/*
    SPDX-License-Identifier: GPL-2.0-only
    SPDX-FileCopyrightText: 2021 Xavier Saliniere <xavier.saliniere@pm.me>
*/

#ifndef KMIXTAPEZ_BACKEND_H
#define KMIXTAPEZ_BACKEND_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkCookie>

const auto host = "https://mymixtapez.com";
const auto user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36";

class Backend : public QObject {
Q_OBJECT
    Q_PROPERTY(bool ready READ ready NOTIFY readyChanged)
    Q_PROPERTY(bool hasPlaylist READ hasPlaylist NOTIFY hasPlaylistChanged)
    Q_PROPERTY(QString playlist READ playlist WRITE setPlaylist NOTIFY playlistChanged)
    Q_PROPERTY(bool shuffle READ shuffle WRITE setShuffle NOTIFY shuffleChanged)

public:
    explicit Backend(QNetworkAccessManager *networkAccessManager) : manager(networkAccessManager) {
        connection = connect(manager, &QNetworkAccessManager::finished,
                             this, [=](QNetworkReply *reply) {
                    if (reply->error()) {
                        qWarning() << reply->errorString();
                        return;
                    }

                    std::string cookieStr;

                    QVariant cookieHeader = reply->header(QNetworkRequest::SetCookieHeader);
                    if (cookieHeader.isValid()) {
                        auto cookies = cookieHeader.value<QList<QNetworkCookie> >();
                                foreach (QNetworkCookie cookie, cookies) {
                                cookieStr += cookie.name().toStdString() + "=" + cookie.value().toStdString() + ";";
                            }
                    }

                    auto verifierStr = extract_verifier(reply->readAll().toStdString());

                    cookie = QString::fromStdString(cookieStr);
                    verifier = QString::fromStdString(verifierStr);

                    disconnect(connection);

                    connection = connect(manager, &QNetworkAccessManager::finished,
                                         this, [=](QNetworkReply *reply) {
                                if (reply->error()) {
                                    qWarning() << reply->errorString();
                                    return;
                                }

                                auto body = reply->readAll();
                                auto location = QString();

                                QVariant locationHeader = reply->header(QNetworkRequest::LocationHeader);
                                if (locationHeader.isValid()) {
                                    location = locationHeader.value<QString>();
                                }

                                Q_EMIT replyReceived(reply->url(), location, body);
                            });

                    m_ready = true;
                    Q_EMIT readyChanged(m_ready);
                }
        );
        request.setUrl(QUrl(host));
        request.setHeader(QNetworkRequest::UserAgentHeader, user_agent);
        manager->get(request);
    }

    [[nodiscard]] bool ready() const;
    [[nodiscard]] bool hasPlaylist() const;
    [[nodiscard]] QString playlist() const;
    [[nodiscard]] bool shuffle() const;

    void setPlaylist(const QString &playlist);
    void setShuffle(const bool &shuffle);

    Q_SIGNAL void readyChanged(bool ready);
    Q_SIGNAL void replyReceived(QUrl url, QString location, QByteArray reply);
    Q_SIGNAL void hasPlaylistChanged();
    Q_SIGNAL void playlistChanged();
    Q_SIGNAL void shuffleChanged();

    Q_INVOKABLE void sendGetRequest(const QString path);

private:
    QString cookie = "";
    QString verifier = "";
    QNetworkAccessManager *manager;
    QNetworkRequest request;
    QMetaObject::Connection connection;
    bool m_ready = false;
    bool m_has_playlist = false;
    QString m_playlist = "[]";
    bool m_shuffle = false;

    static std::string extract_verifier(const std::string &body);
};


#endif //KMIXTAPEZ_BACKEND_H
