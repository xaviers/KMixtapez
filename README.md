# Unmaintained: Please use [Opentapes](https://codeberg.org/xaviers/Opentapes) instead.
# KMixtapez

<p align="center">
<img src="https://img.shields.io/badge/version-0.1.0-blue">
</p>

A Kirigami app for listening to mixtapes, albums & songs hosted on MyMixtapez.com.

<p align="center">
  <img src="https://codeberg.org/xaviers/KMixtapez/media/branch/main/screenshot.png"/>
</p>

## To do
- Add an icon
- Distribute as Flatpack
- Inhibit system sleep while playing a song
- Improve player UI
- Add a search feature
- Browse artists
- Add a cache to store songs location
- Minor fixes/improvements there and there
- A lot more features to implement..

## Dependencies
- Qt5
- Kirigami

## Build from source
### Install required packages (ArchLinux)
```
sudo pacman -Sy base-devel git cmake extra-cmake-modules kirigami2 qt5-multimedia qt5-svg qt5-wayland ki18n --needed
```
### Build
```
git clone https://codeberg.org/xaviers/KMixtapez.git
cd KMixtapez
mkdir cmake-build-release
cmake . -B cmake-build-release -DCMAKE_BUILD_TYPE=Release
cd cmake-build-release
cmake --build . --target kmixtapez
./bin/kmixtapez 
```

## Troubleshooting
### Force mobile mode
Set the QT_QUICK_CONTROLS_MOBILE variable to 1 before running the app.

Example:
```
# Desktop file
Exec=env QT_QUICK_CONTROLS_MOBILE=1 ./kmixtapez
```